module golang.zx2c4.com/wireguard

go 1.15

require (
	github.com/google/go-cmp v0.5.5
	github.com/mikioh/ipaddr v0.0.0-20190404000644-d465c8ab6721
	golang.org/x/net v0.0.0-20201224014010-6772e930b67b
	golang.org/x/sys v0.0.0-20210105210732-16f7687f5001
)
