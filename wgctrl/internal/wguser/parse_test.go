package wguser

import (
	"net"
	"testing"
	"time"

	"github.com/google/go-cmp/cmp"
	"golang.zx2c4.com/wireguard/wgctrl/wgtypes"
)

// Example string source (with some slight modifications to use all fields):
// https://www.wireguard.com/xplatform/#example-dialog.
const okGet = `private_key=01240cd97377d287d75c258204714e8b5d27c4453b40052877ac71923bf8d206e92a6853919d294b739131340e9e476879d711cd7bfea062d2f702997e41a12a382d
listen_port=12912
fwmark=1
public_key=0301fc42b519241594214d4902d170f218704a90838b9255c6da9dcd6e2a552a5a4e2879b58c7db2694a987fd95030d081bbb5c784a77edda442b54d649dc97408e878
preshared_key=188515093e952f5f22e865cef3012e72f8b5f0b598ac0309d5dacce3b70fcf52
allowed_ip=192.168.4.4/32
endpoint=[abcd:23::33%2]:51820
last_handshake_time_sec=1
last_handshake_time_nsec=2
public_key=030053cb7aa78110dbdf3c947c4b03c508735b96e71afe0a29a9826904dfaec86ff4776601b84e01695702f0319fc5b74699544d3c7dcf02b329011d9d3b55a952e2f0
tx_bytes=38333
rx_bytes=2224
allowed_ip=192.168.4.6/32
persistent_keepalive_interval=111
endpoint=182.122.22.19:3233
last_handshake_time_sec=0
last_handshake_time_nsec=0
public_key=02006c771a294a8b4e587022bf95a5644f0fd97327d77843a74bf7536c802fe0831cce473456930e63c72f05e88f6a175a02b7d1932a77bf6e112b6d9adf63a49cf631
endpoint=5.152.198.39:51820
last_handshake_time_sec=0
last_handshake_time_nsec=0
allowed_ip=192.168.4.10/32
allowed_ip=192.168.4.11/32
tx_bytes=1212111
rx_bytes=1929999999
protocol_version=1
errno=0

`

func TestClientDevices(t *testing.T) {
	// Used to trigger "parse peers" mode easily.
	const okKey = "public_key=000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000\n"

	tests := []struct {
		name string
		res  []byte
		ok   bool
		d    *wgtypes.Device
	}{
		{
			name: "invalid key=value",
			res:  []byte("foo=bar=baz"),
		},
		{
			name: "invalid public_key",
			res:  []byte("public_key=xxx"),
		},
		{
			name: "short public_key",
			res:  []byte("public_key=abcd"),
		},
		{
			name: "invalid fwmark",
			res:  []byte("fwmark=foo"),
		},
		{
			name: "invalid endpoint",
			res:  []byte(okKey + "endpoint=foo"),
		},
		{
			name: "invalid allowed_ip",
			res:  []byte(okKey + "allowed_ip=foo"),
		},
		{
			name: "error",
			res:  []byte("errno=2\n\n"),
		},
		{
			name: "ok",
			res:  []byte(okGet),
			ok:   true,
			d: &wgtypes.Device{
				Name:         testDevice,
				Type:         wgtypes.Userspace,
				PrivateKey:   wgtypes.Key{0x01, 0x24, 0x0c, 0xd9, 0x73, 0x77, 0xd2, 0x87, 0xd7, 0x5c, 0x25, 0x82, 0x04, 0x71, 0x4e, 0x8b, 0x5d, 0x27, 0xc4, 0x45, 0x3b, 0x40, 0x05, 0x28, 0x77, 0xac, 0x71, 0x92, 0x3b, 0xf8, 0xd2, 0x06, 0xe9, 0x2a, 0x68, 0x53, 0x91, 0x9d, 0x29, 0x4b, 0x73, 0x91, 0x31, 0x34, 0x0e, 0x9e, 0x47, 0x68, 0x79, 0xd7, 0x11, 0xcd, 0x7b, 0xfe, 0xa0, 0x62, 0xd2, 0xf7, 0x02, 0x99, 0x7e, 0x41, 0xa1, 0x2a, 0x38, 0x2d},
				PublicKey:    wgtypes.Key{0x02, 0x00, 0x28, 0xf2, 0x86, 0x01, 0xfa, 0x74, 0xd7, 0xf6, 0xd7, 0x16, 0x5f, 0xd5, 0x02, 0xaf, 0x50, 0xf7, 0x00, 0x23, 0x8b, 0xcd, 0x95, 0x7b, 0x32, 0x91, 0xc9, 0x95, 0xf4, 0x9f, 0x60, 0xf8, 0x0f, 0x89, 0x2c, 0x88, 0x09, 0x92, 0x1d, 0xff, 0x56, 0x14, 0x28, 0xe5, 0xbe, 0xba, 0x36, 0x84, 0x07, 0x05, 0xb1, 0xe4, 0x47, 0x21, 0x56, 0x14, 0xf1, 0xe2, 0xe4, 0x37, 0x02, 0xcb, 0x95, 0xa4, 0xea, 0x03, 0x0b},
				ListenPort:   12912,
				FirewallMark: 1,
				Peers: []wgtypes.Peer{
					{
						PublicKey:    wgtypes.Key{0x03, 0x01, 0xfc, 0x42, 0xb5, 0x19, 0x24, 0x15, 0x94, 0x21, 0x4d, 0x49, 0x02, 0xd1, 0x70, 0xf2, 0x18, 0x70, 0x4a, 0x90, 0x83, 0x8b, 0x92, 0x55, 0xc6, 0xda, 0x9d, 0xcd, 0x6e, 0x2a, 0x55, 0x2a, 0x5a, 0x4e, 0x28, 0x79, 0xb5, 0x8c, 0x7d, 0xb2, 0x69, 0x4a, 0x98, 0x7f, 0xd9, 0x50, 0x30, 0xd0, 0x81, 0xbb, 0xb5, 0xc7, 0x84, 0xa7, 0x7e, 0xdd, 0xa4, 0x42, 0xb5, 0x4d, 0x64, 0x9d, 0xc9, 0x74, 0x08, 0xe8, 0x78},
						PresharedKey: wgtypes.Key{0x18, 0x85, 0x15, 0x9, 0x3e, 0x95, 0x2f, 0x5f, 0x22, 0xe8, 0x65, 0xce, 0xf3, 0x1, 0x2e, 0x72, 0xf8, 0xb5, 0xf0, 0xb5, 0x98, 0xac, 0x3, 0x9, 0xd5, 0xda, 0xcc, 0xe3, 0xb7, 0xf, 0xcf, 0x52},
						Endpoint: &net.UDPAddr{
							IP:   net.ParseIP("abcd:23::33"),
							Port: 51820,
							Zone: "2",
						},
						LastHandshakeTime: time.Unix(1, 2),
						AllowedIPs: []net.IPNet{
							{
								IP:   net.IP{0xc0, 0xa8, 0x4, 0x4},
								Mask: net.IPMask{0xff, 0xff, 0xff, 0xff},
							},
						},
					},
					{
						PublicKey: wgtypes.Key{0x03, 0x00, 0x53, 0xcb, 0x7a, 0xa7, 0x81, 0x10, 0xdb, 0xdf, 0x3c, 0x94, 0x7c, 0x4b, 0x03, 0xc5, 0x08, 0x73, 0x5b, 0x96, 0xe7, 0x1a, 0xfe, 0x0a, 0x29, 0xa9, 0x82, 0x69, 0x04, 0xdf, 0xae, 0xc8, 0x6f, 0xf4, 0x77, 0x66, 0x01, 0xb8, 0x4e, 0x01, 0x69, 0x57, 0x02, 0xf0, 0x31, 0x9f, 0xc5, 0xb7, 0x46, 0x99, 0x54, 0x4d, 0x3c, 0x7d, 0xcf, 0x02, 0xb3, 0x29, 0x01, 0x1d, 0x9d, 0x3b, 0x55, 0xa9, 0x52, 0xe2, 0xf0},
						Endpoint: &net.UDPAddr{
							IP:   net.IPv4(182, 122, 22, 19),
							Port: 3233,
						},
						// Zero-value because UNIX timestamp of 0. Explicitly
						// set for documentation purposes here.
						LastHandshakeTime:           time.Time{},
						PersistentKeepaliveInterval: 111000000000,
						ReceiveBytes:                2224,
						TransmitBytes:               38333,
						AllowedIPs: []net.IPNet{
							{
								IP:   net.IP{0xc0, 0xa8, 0x4, 0x6},
								Mask: net.IPMask{0xff, 0xff, 0xff, 0xff},
							},
						},
					},
					{
						PublicKey: wgtypes.Key{0x02, 0x00, 0x6c, 0x77, 0x1a, 0x29, 0x4a, 0x8b, 0x4e, 0x58, 0x70, 0x22, 0xbf, 0x95, 0xa5, 0x64, 0x4f, 0x0f, 0xd9, 0x73, 0x27, 0xd7, 0x78, 0x43, 0xa7, 0x4b, 0xf7, 0x53, 0x6c, 0x80, 0x2f, 0xe0, 0x83, 0x1c, 0xce, 0x47, 0x34, 0x56, 0x93, 0x0e, 0x63, 0xc7, 0x2f, 0x05, 0xe8, 0x8f, 0x6a, 0x17, 0x5a, 0x02, 0xb7, 0xd1, 0x93, 0x2a, 0x77, 0xbf, 0x6e, 0x11, 0x2b, 0x6d, 0x9a, 0xdf, 0x63, 0xa4, 0x9c, 0xf6, 0x31},
						Endpoint: &net.UDPAddr{
							IP:   net.IPv4(5, 152, 198, 39),
							Port: 51820,
						},
						ReceiveBytes:  1929999999,
						TransmitBytes: 1212111,
						AllowedIPs: []net.IPNet{
							{
								IP:   net.IP{0xc0, 0xa8, 0x4, 0xa},
								Mask: net.IPMask{0xff, 0xff, 0xff, 0xff},
							},
							{
								IP:   net.IP{0xc0, 0xa8, 0x4, 0xb},
								Mask: net.IPMask{0xff, 0xff, 0xff, 0xff},
							},
						},
						ProtocolVersion: 1,
					},
				},
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c, done := testClient(t, tt.res)
			defer done()

			devs, err := c.Devices()

			if tt.ok && err != nil {
				t.Fatalf("failed to get devices: %v", err)
			}
			if !tt.ok && err == nil {
				t.Fatal("expected an error, but none occurred")
			}
			if err != nil {
				return
			}

			if diff := cmp.Diff([]*wgtypes.Device{tt.d}, devs); diff != "" {
				t.Fatalf("unexpected Devices (-want +got):\n%s", diff)
			}
		})
	}
}
